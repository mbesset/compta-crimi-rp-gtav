// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBGhTtRYP_ZvCq4cNgJixsjyZaG8h7NHgo",
    authDomain: "compta-crimi-rp-gta5.firebaseapp.com",
    projectId: "compta-crimi-rp-gta5",
    storageBucket: "compta-crimi-rp-gta5.appspot.com",
    messagingSenderId: "358151915550",
    appId: "1:358151915550:web:6310fccab8f34edcb1460f",
    measurementId: "G-5ES8358LGH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
