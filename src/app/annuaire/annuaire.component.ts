import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { AnnuaireModel } from '../models/annuaire.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DeletePopupComponent } from '../utils/delete-popup/delete-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { EditPopupComponent } from './edit-popup/edit-popup.component';
import { DatabaseService } from '../services/db.service';

@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.component.html',
  styleUrls: ['./annuaire.component.scss'],
})
export class AnnuaireComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns: string[] = [
    'name',
    'surName',
    'phone',
    'percentDeal',
    'other',
    'status',
    'comment',
    'actif',
    'actions',
  ];
  dataSource = new MatTableDataSource<AnnuaireModel>([]);

  constructor(
    public dialog: MatDialog,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.updateTable();
  }

  ngAfterViewInit() {}

  /**
   * Open Dialog for add contact
   */
  public add() {
    const data: AnnuaireModel = new AnnuaireModel();
    const dialogRef = this.dialog.open(EditPopupComponent, {
      width: '800px',
      data: {
        data: data,
        title: "Création d'un contact",
        message: '',
      },
    });

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        data.id = (data.name.trim() + '_' + data.surName.trim())
          .trim()
          .toLowerCase();
        data.name = data.name.trim();
        data.surName = data.surName.trim();
        this.databaseService.addDocumentWithCustomId('annuaire', { ...data });
      }
    });
  }

  /**
   * Open Dialog for edit contact
   * @param element
   */
  public edit(element: AnnuaireModel) {
    const dialogRef = this.dialog.open(EditPopupComponent, {
      width: '800px',
      data: {
        data: element,
        title: "Edition d'un contact",
        message: '',
      },
    });

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        this.databaseService.updateDocument('annuaire', data);
      }
    });
  }

  /**
   * Delete Document
   * @param element
   */
  public delete(element: AnnuaireModel) {
    const dialogRef = this.dialog.open(DeletePopupComponent, {
      width: '400px',
      data: {
        data: element,
        title: "Suppresion d'un contact",
        message:
          'Etes vous sur de vouloir supprimer : ' +
          element.name +
          ' ' +
          element.surName,
      },
    });

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        this.databaseService.deleteDocument('annuaire', data);
      }
    });
  }

  /**
   * Update Table DATA
   */
  private updateTable() {
    this.databaseService.getDatabase('annuaire').subscribe((result: any) => {
      this.dataSource = new MatTableDataSource<AnnuaireModel>(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  /**
   * Apply Filter on table
   * @param event
   */
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
