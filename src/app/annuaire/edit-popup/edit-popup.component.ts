import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatabaseService } from '../../services/db.service';

export interface DialogData {
  data: any;
  title: string;
  message: string;
}

@Component({
  selector: 'app-edit-popup',
  templateUrl: './edit-popup.component.html',
  styleUrls: ['./edit-popup.component.scss'],
})
export class EditPopupComponent implements OnInit {
  public title: string = '';
  public status: string[] = [];
  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public inputData: DialogData,
    private databaseService: DatabaseService
  ) { }

  ngOnInit(): void {
    this.title = this.inputData.title;
    this.databaseService.getGrades().subscribe((grades: any) => {
      this.status = grades.data;
    })

  }
}
