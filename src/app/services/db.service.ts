import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable()
export class DatabaseService {
  db: AngularFirestore;

  dbConnector = undefined;
  constructor(_db: AngularFirestore) {
    this.db = _db;
  }

  public createdbs() {}

  private checkdatabases() {}

  /**
   * Return all doc on specific database
   * @param target
   * @returns
   */
  public getDatabase(database: string): Observable<any> {
    return this.db.collection(database).valueChanges({ idField: 'id' });
  }

  /**
   * Get specific document on specific database
   * @param database
   * @param documentId
   * @returns
   */
  public getDocument(database: string, documentId: string): Observable<any> {
    const _database: AngularFirestoreCollection = this.db.collection(database);
    return _database.doc(documentId).valueChanges({ idField: 'id' });
  }

  /**
   *
   * @param database
   * @param data
   * @returns
   */
  public updateDocument(database: string, data: any, id?: string) {
    const _database: AngularFirestoreCollection = this.db.collection(database);
    const _id = id ? id : data.id;
    return _database
      .doc(_id)
      .set(data)
      .catch((error) => {
        console.error(error);
      });
  }

  /**
   *
   * @param database
   * @param data
   * @returns
   */
  public addDocumentWithCustomId(database: string, data: any, id?: string) {
    const _database: AngularFirestoreCollection = this.db.collection(database);
    const _id = id ? id : data.id;
    return _database
      .doc(_id)
      .set(data)
      .catch((error) => {
        console.error(error);
      });
  }

  /**
   *
   * @param database
   * @param data
   * @returns
   */
  public addDocument(database: string, data: any) {
    const _database: AngularFirestoreCollection = this.db.collection(database);

    return _database.add({ ...data }).catch((error) => {
      console.error(error);
    });
  }

  /**
   *
   * @param database
   * @param data
   * @returns
   */
  public deleteDocument(database: string, data: any) {
    const _database: AngularFirestoreCollection = this.db.collection(database);
    return _database
      .doc(data.id)
      .delete()
      .catch((err) => {
        console.error(err);
      });
  }

  public getCollectionWithFilter(
    database: string,
    keySearch: string,
    arguement: any,
    valueSearch: any
  ) {
    return this.db
      .collection(database, (ref) =>
        ref.where(keySearch, arguement, valueSearch)
      )
      .valueChanges({ idField: 'id' });
  }

  /**
   * Get Grade
   * @returns
   */
  public getGrades(): Observable<any> {
    return this.getDocument('gestions', 'grades');
  }

  /**
   * Get business
   * @returns
   */
  public getBusiness(): Observable<any> {
    return this.getDocument('gestions', 'business');
  }

  /**
   * Get supplier
   * @returns
   */
  public getSupplier(): Observable<any> {
    return this.getDocument('gestions', 'supplier');
  }

  /**
   * Get launderer
   * @returns
   */
  public getLaunderer(): Observable<any> {
    return this.getDocument('gestions', 'launderer');
  }

  /**
   * Get launderer
   * @returns
   */
  public getSettings(): Observable<any> {
    return this.getDocument('utils', 'settings');
  }
}
