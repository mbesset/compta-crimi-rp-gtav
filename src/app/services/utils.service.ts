import { Injectable } from '@angular/core';
import { promises } from 'dns';
import { Observable } from 'rxjs';
import { DatabaseService } from './db.service';

@Injectable()
export class UtilsService {
  constructor(private databaseService: DatabaseService) {}

  public getMembre(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.databaseService
        .getCollectionWithFilter('annuaire', 'status', '==', 'Membre')
        .subscribe((users) => {
          resolve(
            users.map((user: any) => {
              return user.name + ' ' + user.surName;
            })
          );
        });
    });
  }
}
