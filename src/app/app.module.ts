import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatabaseService } from './services/db.service';
import { UtilsService } from './services/utils.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularDocComponent } from './angular-doc/angular-doc.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { AnnuaireComponent } from './annuaire/annuaire.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { DeletePopupComponent } from './utils/delete-popup/delete-popup.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EditPopupComponent } from './annuaire/edit-popup/edit-popup.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MenuComponent } from './utils/menu/menu.component';
import { MatListModule } from '@angular/material/list';
import { ActivityComponent } from './activity/activity.component';
import { ActivityAddPopComponent } from './activity/activity-add-pop/activity-add-pop.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { PaymentAddPopComponent } from './activity/payment-add-pop/payment-add-pop.component';
import { HeaderComponent } from './utils/header/header.component';
import { DayHistoricComponent } from './statistiques/day-historic/day-historic.component';
import { GestionsComponent } from './gestions/gestions.component';
import { SettingsComponent } from './settings/settings.component';
import { ComptabilityComponent } from './comptability/compatibity.component';
import { GestionsArrayDisplayComponent } from './gestions/gestions-array-display/gestions-array-display.component';
import { MatCardModule } from '@angular/material/card';
import { PurchasePriceEditPopComponent } from './comptability/purchase-price-edit-pop/purchase-price-edit-pop.component';
import { PurchasePopComponent } from './comptability/purchase-pop/purchase-pop.component';
import { LaunderingPopComponent } from './comptability/laundering-pop/laundering-pop.component';
import { BleachingRatePopComponent } from './comptability/bleaching-rate-pop/bleaching-rate-pop.component';
import { SalePopComponent } from './comptability/sale-pop/sale-pop.component';
import { StorageComponent } from './storage/storage.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StorageAddPopComponent } from './storage/storage-add-pop/storage-add-pop.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AngularDocComponent,
    AnnuaireComponent,
    DeletePopupComponent,
    EditPopupComponent,
    MenuComponent,
    ActivityComponent,
    ActivityAddPopComponent,
    PaymentAddPopComponent,
    HeaderComponent,
    DayHistoricComponent,
    GestionsComponent,
    SettingsComponent,
    ComptabilityComponent,
    GestionsArrayDisplayComponent,
    PurchasePriceEditPopComponent,
    PurchasePopComponent,
    LaunderingPopComponent,
    BleachingRatePopComponent,
    SalePopComponent,
    StorageComponent,
    StorageAddPopComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSortModule,
    MatSidenavModule,
    MatListModule,
    MatAutocompleteModule,
    MatCardModule,
    MatSnackBarModule
  ],
  providers: [
    DatabaseService,
    UtilsService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
