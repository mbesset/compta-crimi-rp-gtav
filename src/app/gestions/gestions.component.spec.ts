import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionsComponent } from './gestions.component';

describe('GestionsComponent', () => {
  let component: GestionsComponent;
  let fixture: ComponentFixture<GestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
