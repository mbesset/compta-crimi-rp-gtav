import { Component, OnInit } from '@angular/core';
import { GestionDatabaseInfo, DataModels } from '../models/gestions.models'
import { DatabaseService } from '../services/db.service'

@Component({
  selector: 'app-gestions',
  templateUrl: './gestions.component.html',
  styleUrls: ['./gestions.component.scss']
})
export class GestionsComponent implements OnInit {
  public gradesDatabaseInfo: GestionDatabaseInfo = {
    database: "gestions",
    document: "grades",
    data: []
  }

  public fournisseurDatabaseInfo: GestionDatabaseInfo = {
    database: "gestions",
    document: "supplier",
    data: []
  }

  public businessDatabaseInfo: GestionDatabaseInfo = {
    database: "gestions",
    document: "business",
    data: []
  }

  public laundererDatabaseInfo: GestionDatabaseInfo = {
    database: "gestions",
    document: "launderer",
    data: []
  }


  constructor(
    private databaseService: DatabaseService
  ) { }

  async ngOnInit() {
    Promise.all([
      this.getData(this.gradesDatabaseInfo),
      this.getData(this.businessDatabaseInfo),
      this.getData(this.fournisseurDatabaseInfo),
      this.getData(this.laundererDatabaseInfo)
    ])
  }

  public add(target: GestionDatabaseInfo) {
    target.data.push({
      name: "",
      edit: true
    });
  }

  private getData(databaseInfo: GestionDatabaseInfo) {
    this.databaseService.getDocument(
      databaseInfo.database,
      databaseInfo.document
    ).subscribe((output) => {
      if (output) {
        databaseInfo.data = this.createDataObject(output.data);
      }
    })
  }

  private createDataObject(input: string[]): DataModels[] {
    return input.map((_data) => {
      return {
        name: _data,
        edit: false
      }
    })
  }
}
