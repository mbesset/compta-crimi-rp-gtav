import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionsArrayDisplayComponent } from './gestions-array-display.component';

describe('GestionsArrayDisplayComponent', () => {
  let component: GestionsArrayDisplayComponent;
  let fixture: ComponentFixture<GestionsArrayDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionsArrayDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionsArrayDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
