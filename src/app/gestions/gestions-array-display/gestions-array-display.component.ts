import { Component, Input, OnInit } from '@angular/core';
import { GestionDatabaseInfo } from '../../models/gestions.models';
import { DatabaseService } from '../../services/db.service'

@Component({
  selector: 'app-gestions-array-display',
  templateUrl: './gestions-array-display.component.html',
  styleUrls: ['./gestions-array-display.component.scss']
})
export class GestionsArrayDisplayComponent implements OnInit {
  @Input() input: GestionDatabaseInfo = new GestionDatabaseInfo;
  constructor(
    private databaseService: DatabaseService
  ) { }

  ngOnInit(): void {

  }

  public delete(data: any) {
    this.input.data = this.input.data.filter((ele: any) => {
      return ele != data;
    });
    this.rebuiltData();
  }

  public update(data: any) {
    data.edit = false;
    data.name = data.name.trim();
    this.rebuiltData();
  }

  private rebuiltData() {
    const nameArray = this.input.data.map((_data: any) => {
      return _data.name;
    });
    this.databaseService.updateDocument(this.input.database, { 'data': nameArray }, this.input.document)

  }
}
