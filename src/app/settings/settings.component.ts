import { Component, OnInit } from '@angular/core';
import { SettingsModel } from '../models/settings.models';
import { DatabaseService } from '../services/db.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  public settings: SettingsModel = new SettingsModel();
  constructor(private databaseService: DatabaseService) {}

  ngOnInit(): void {
    this.databaseService.getSettings().subscribe((settings: any) => {
      this.settings = {
        ...this.settings,
        ...settings,
      };
    });
  }

  public save() {
    this.databaseService.updateDocument(
      'utils',
      { ...this.settings },
      this.settings.id
    );
  }
}
