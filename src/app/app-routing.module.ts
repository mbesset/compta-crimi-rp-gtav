import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularDocComponent } from './angular-doc/angular-doc.component';
import { AnnuaireComponent } from './annuaire/annuaire.component';
import { ComptabilityComponent } from './comptability/compatibity.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GestionsComponent } from './gestions/gestions.component';
import { SettingsComponent } from './settings/settings.component';
import { StorageComponent } from './storage/storage.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: "full"
  },
  {
    path: "dashboard",
    component: DashboardComponent
  },
  {
    path: "doc",
    component: AngularDocComponent
  },
  {
    path: "annuaire",
    component: AnnuaireComponent
  },
  {
    path: "gestions",
    component: GestionsComponent
  },
  {
    path: "comptability",
    component: ComptabilityComponent
  },
  {
    path: "storage",
    component: StorageComponent
  },
  {
    path: "settings",
    component: SettingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
