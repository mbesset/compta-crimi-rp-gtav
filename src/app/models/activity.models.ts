export class ActivityModel {
    id: string;
    name: string;
    quantity: string;
    types: string;
    createAt: any;
    actif: boolean;
    dirtyMoney: number;
    paid: number;
    superetteBag: number;
    paidBy: string;


    constructor() {
        this.id = "";
        this.name = "";
        this.quantity = "20";
        this.types = "555-";
        this.createAt = undefined;
        this.actif = true;
        this.dirtyMoney = 0;
        this.paid = 0;
        this.superetteBag = 0;
        this.paidBy = '';
    }
}