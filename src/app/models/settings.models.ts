export class SettingsModel {
  id: string;
  logoPath: string;
  webSiteTitle: string;
  slogan: string;

  constructor() {
    this.id = '';
    this.logoPath = './assets/img/image-not-found.jpg';
    this.webSiteTitle = 'Nom du gang';
    this.slogan = 'Gestion & Comptabilité';
  }
}
