export class GestionDatabaseInfo {
    database: string;
    document: string;
    data: DataModels[];

    constructor() {
        this.database = "";
        this.document = "";
        this.data = [];
    }
}

export class DataModels {
    name: string;
    edit: boolean;

    constructor() {
        this.name = "";
        this.edit = false;
    }
}