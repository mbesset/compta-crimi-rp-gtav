import { timeStamp } from "console";

export class StorageModel {
    id: string;
    storageName: string;
    maxStorage: number;
    stock: StockModel[];
    tenant: TenantModel[]


    constructor() {
        this.id = "";
        this.storageName = "";
        this.maxStorage = 0;
        this.stock = [];
        this.tenant = [];
    }
}

export class StorageModelPop {
    title: string;
    data: StorageModel

    constructor() {
        this.title = "";
        this.data = new StorageModel();
    }
}

export class TenantModel{
    name : string;
    edit ?: boolean;

    constructor(){
        this.name ="";
    }
}

export interface StockModel2 {
    id: string;
    storageName: string;
    maxStorage: number;
    stock: any;
    edit: boolean;
}

export interface StockMapModel {
    key: string;
    value: StockModel;
}

export class StockModel {
    name: string;
    quantity: number;
    edit?: boolean

    constructor() {
        this.name = "";
        this.quantity = 0;
        this.edit = true;
    }
}