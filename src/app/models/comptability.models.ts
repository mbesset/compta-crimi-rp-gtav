import { formatDate } from "@angular/common";

export class PurchasePriceModel {
  id: string;
  supplier: string;
  unitPrice: number;
  product: string;

  constructor() {
    this.id = '';
    this.supplier = '';
    this.unitPrice = 0;
    this.product = '';
  }
}

export class PurchaseModel {
  id: string;
  supplier: string;
  quantity: number;
  product: string;
  totalPrice: number;
  paidBy: string;
  unitPrice: number;
  date: string;

  constructor() {
    this.id = '';
    this.supplier = '';
    this.unitPrice = 0;
    this.product = '';
    this.paidBy = '';
    this.quantity = 0;
    this.totalPrice = 0;
    this.date = formatDate(new Date(), 'yyyy/MM/dd', 'en');
  }
}

export class BleachingRateModel {
  id: string;
  launderer: string;
  rateDirtyMoney: number;
  bagPrice: number;

  constructor() {
    this.id = '';
    this.launderer = '';
    this.rateDirtyMoney = 0;
    this.bagPrice = 0;
  }
}

export class BleachingRatePopModel {
  title: string;
  data: BleachingRateModel;

  constructor() {
    this.title = '';
    this.data = new BleachingRateModel();
  }
}

export class LaunderingModel {
  id: string;
  date: string;
  launderer: string;
  dirtyMoney: number;
  rateDirtyMoney: number;
  totalDirtyMoney: number;
  bag: number;
  bagPrice: number;
  totalBag: number;
  totalPrice: number;

  constructor() {
    this.id = '';
    this.launderer = '';
    this.date = formatDate(new Date(), 'yyyy/MM/dd', 'en');;
    this.dirtyMoney = 0;
    this.rateDirtyMoney = 0;
    this.totalDirtyMoney = 0;
    this.bag = 0;
    this.bagPrice = 0;
    this.totalBag = 0;
    this.totalPrice = 0;
  }
}

export class LaunderingPopModel {
  title: string;
  data: LaunderingModel;

  constructor() {
    this.title = '';
    this.data = new LaunderingModel();
  }
}

export class SaleModel {
  id: string;
  name: string;
  product: string;
  quantity: number;
  price: number;
  total: number;
  date: string;

  constructor() {
    this.id = '';
    this.name = '';
    this.product = '';
    this.quantity = 0;
    this.price = 0;
    this.total = 0;
    this.date = formatDate(new Date(), 'yyyy/MM/dd', 'en');
  }
}

export class SaleModelPop {
  title: string;
  data: SaleModel;

  constructor() {
    this.title = '';
    this.data = new SaleModel();
  }
}
