export class AnnuaireModel{
    id: string;
    name : string;
    surName: string;
    phone: string;
    status: string;
    actif : boolean;
    percentDeal: number;
    other: number;
    comment: string;

    constructor(){
        this.id = "";
        this.name ="";
        this.surName = "";
        this.phone = "555-";
        this.status = "Petite Main";
        this.actif = true;
        this.percentDeal = 20;
        this.other = 50;
        this.comment = "";
    }
}