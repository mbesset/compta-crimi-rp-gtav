import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaunderingPopComponent } from './laundering-pop.component';

describe('LaundererPopComponent', () => {
  let component: LaunderingPopComponent;
  let fixture: ComponentFixture<LaunderingPopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaunderingPopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunderingPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
