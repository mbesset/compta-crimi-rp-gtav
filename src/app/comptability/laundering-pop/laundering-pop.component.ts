import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  LaunderingModel,
  LaunderingPopModel,
} from 'src/app/models/comptability.models';
import { DatabaseService } from 'src/app/services/db.service';

@Component({
  selector: 'app-launderer-pop',
  templateUrl: './laundering-pop.component.html',
  styleUrls: ['./laundering-pop.component.scss'],
})
export class LaunderingPopComponent implements OnInit {
  public title: string = '';
  public launderer: string[] = [];
  public _supplierByBusiness: string[] = [];
  public business: string[] = [];
  public purchasePriceTab: any[] = [];

  public profileForm = new FormGroup({
    laundererValidator: new FormControl('', [Validators.required]),
    dirtyMoneyValidator: new FormControl('', [Validators.required]),
    rateDirtyMoneyValidator: new FormControl('', [Validators.required]),
    totalDirtyMoneyValidator: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    bagValidator: new FormControl('', [Validators.required]),
    bagPriceValidator: new FormControl('', [Validators.required]),
    totalSacValidator: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    totalPriceValidator: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
  });
  public purchasePriceDataSource: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public inputData: LaunderingPopModel,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.databaseService.getLaunderer().subscribe((output) => {
      this.launderer = output.data;
    });
  }

  public getRateByLaunderer() {
    this.databaseService
      .getCollectionWithFilter(
        'bleachingRate',
        'launderer',
        '==',
        this.inputData.data.launderer
      )
      .subscribe((_bleachingRate: any) => {
        this.inputData.data.rateDirtyMoney = _bleachingRate[0].rateDirtyMoney;
        this.inputData.data.bagPrice = _bleachingRate[0].bagPrice;
      });
  }

  public calcDirtyMoney(): void {
    this.inputData.data.totalDirtyMoney =
      this.inputData.data.dirtyMoney *
      ((100 - this.inputData.data.rateDirtyMoney) / 100);
    this.calculTotal();
  }

  public calcSac(): void {
    this.inputData.data.totalBag =
      this.inputData.data.bag * this.inputData.data.bagPrice;
    this.calculTotal();
  }

  public calculTotal(): number {
    return (this.inputData.data.totalPrice =
      this.inputData.data.totalBag + this.inputData.data.totalDirtyMoney);
  }
}
