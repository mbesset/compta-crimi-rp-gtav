import { Component, Inject, OnInit } from '@angular/core';
import { DatabaseService } from '../../services/db.service'
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-purchase-price-edit-pop',
  templateUrl: './purchase-price-edit-pop.component.html',
  styleUrls: ['./purchase-price-edit-pop.component.scss']
})
export class PurchasePriceEditPopComponent implements OnInit {
  public suppliers: string[] = [];
  public business: string[] = [];
  public title: string = "";

  public profileForm = new FormGroup({
    supplierValidator: new FormControl('', [Validators.required]),
    unitPriceValidator: new FormControl('', [Validators.required]),
    productValidator: new FormControl('', [Validators.required])
  });


  constructor(
    @Inject(MAT_DIALOG_DATA) public inputData: any,
    private databaseService: DatabaseService
  ) { }

  ngOnInit(): void {
    this.profileForm.patchValue(
      {
        supplierValidator: this.inputData.data.supplier,
        unitPriceValidator: this.inputData.data.unitPrice,
        productValidator: this.inputData.data.product
      });
    this.title = this.inputData.title

    this.databaseService.getSupplier().subscribe((output) => {
      this.suppliers = output.data;
    });

    this.databaseService.getBusiness().subscribe((output) => {
      this.business = output.data;
    });
  }
}
