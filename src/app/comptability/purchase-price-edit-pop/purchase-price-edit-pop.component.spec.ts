import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasePriceEditPopComponent } from './purchase-price-edit-pop.component';

describe('PurchasePriceEditPopComponent', () => {
  let component: PurchasePriceEditPopComponent;
  let fixture: ComponentFixture<PurchasePriceEditPopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchasePriceEditPopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasePriceEditPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
