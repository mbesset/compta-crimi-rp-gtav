import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalePopComponent } from './sale-pop.component';

describe('SalePopComponent', () => {
  let component: SalePopComponent;
  let fixture: ComponentFixture<SalePopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalePopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalePopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
