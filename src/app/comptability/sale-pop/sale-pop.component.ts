import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { DatabaseService } from 'src/app/services/db.service';
import { map, startWith } from 'rxjs/operators';
import { SaleModelPop } from 'src/app/models/comptability.models';

@Component({
  selector: 'app-sale-pop',
  templateUrl: './sale-pop.component.html',
  styleUrls: ['./sale-pop.component.scss'],
})
export class SalePopComponent implements OnInit {
  public title: string = '';
  public types: string[] = [];
  public users: string[] = [];
  filteredOptions!: Observable<string[]>;

  public saleForm = new FormGroup({
    nameValidator: new FormControl('', [Validators.required]),
    productValidator: new FormControl('', [Validators.required]),
    quantityValidator: new FormControl('', [Validators.required]),
    priceValidator: new FormControl('', [Validators.required]),
    totalValidator: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
  });

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public inputData: SaleModelPop,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.title = this.inputData.title;

    this.databaseService.getDatabase('annuaire').subscribe((result) => {
      this.users = result.map((e: any) => {
        return e.name + ' ' + e.surName;
      });

      this.filteredOptions = this.saleForm
        .get('nameValidator')
        ?.valueChanges.pipe(
          startWith(''),
          map((value) => this._filter(value))
        ) as Observable<string[]>;
    });

    this.databaseService.getBusiness().subscribe((result: any) => {
      this.types = result.data;
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.users.filter((users) =>
      users.toLowerCase().includes(filterValue)
    );
  }

  public calcTotal(): number {
    return (this.inputData.data.total =
      this.inputData.data.price * this.inputData.data.quantity);
  }
}
