import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PurchasePriceEditPopComponent } from './purchase-price-edit-pop/purchase-price-edit-pop.component';
import {
  PurchasePriceModel,
  PurchaseModel,
  BleachingRateModel,
  LaunderingModel,
  SaleModel,
} from '../models/comptability.models';
import { DatabaseService } from '../services/db.service';
import { PurchasePopComponent } from './purchase-pop/purchase-pop.component';
import { formatDate } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { LaunderingPopComponent } from './laundering-pop/laundering-pop.component';
import { BleachingRatePopComponent } from './bleaching-rate-pop/bleaching-rate-pop.component';
import { SalePopComponent } from './sale-pop/sale-pop.component';

@Component({
  selector: 'app-compatibity',
  templateUrl: './comptability.component.html',
  styleUrls: ['./comptability.component.scss'],
})
export class ComptabilityComponent implements OnInit {
  public tests = [];
  public purchasePrice: PurchasePriceModel[] = [];
  public purchasePriceDataSource: PurchasePriceModel[] = [];
  public purchaseDisplayedSource = new MatTableDataSource<PurchaseModel>([]);
  public laundererDisplayedSource = new MatTableDataSource<any>([]);
  public bleachingRateDisplayedSource = new MatTableDataSource<any>([]);
  public saleDisplayedSource = new MatTableDataSource<any>([]);
  public purchasePriceDisplayedColumns: string[] = [
    'supplier',
    'product',
    'unitPrice',
    'actions',
  ];
  public purchaseDisplayedColumns: string[] = [
    'date',
    'supplier',
    'product',
    'quantity',
    'totalPaid',
    'paidBy',
  ];
  public leachingRateDisplayedColumns: string[] = [
    'launderer',
    'rateDirtyMoney',
    'bagPrice',
    'actions',
  ];

  public laundererDisplayedColumns: string[] = [
    'date',
    'launderer',
    'dirtyMoney',
    'bag',
    'totalPrice',
  ];

  public saleDisplayedColumns: string[] = [
    'date',
    'name',
    'product',
    'quantity',
    'price',
    'total',
  ];

  @ViewChild('purchasePaginator') purchasePaginator!: MatPaginator;
  @ViewChild('laundererPaginator') laundererPaginator!: MatPaginator;

  constructor(
    private databaseService: DatabaseService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.updateTable();
  }

  private updateTable() {
    this.databaseService
      .getDatabase('purchasePrice')
      .subscribe((_purchasePrice: PurchasePriceModel[]) => {
        this.purchasePriceDataSource = _purchasePrice;
      });

    this.databaseService
      .getDatabase('purchase')
      .subscribe((_purchase: PurchaseModel[]) => {
        this.laundererDisplayedSource = this.purchaseDisplayedSource =
          new MatTableDataSource<PurchaseModel>(_purchase.reverse());
        this.purchaseDisplayedSource.paginator = this.purchasePaginator;
      });

    this.databaseService
      .getDatabase('laundering')
      .subscribe((_launderer: PurchaseModel[]) => {
        this.laundererDisplayedSource = new MatTableDataSource<any>(
          _launderer.reverse()
        );
        this.laundererDisplayedSource.paginator = this.laundererPaginator;
      });

    this.databaseService
      .getDatabase('bleachingRate')
      .subscribe((_bleachingRate: PurchaseModel[]) => {
        this.bleachingRateDisplayedSource = new MatTableDataSource<any>(
          _bleachingRate
        );
      });

    this.databaseService
      .getDatabase('sale')
      .subscribe((_sale: PurchaseModel[]) => {
        this.saleDisplayedSource = new MatTableDataSource<any>(_sale.reverse());
      });
  }

  public deleteDocument(database: string, element: string) {
    this.databaseService.deleteDocument(database, element);
  }

  public addOrEditPurchasePrice(data?: PurchasePriceModel) {
    let title = "Edition d'un prix d'achat";
    if (!data) {
      data = new PurchasePriceModel();
      title = "Ajout d'un prix d'achat";
    }
    const dialogRef = this.dialog.open(PurchasePriceEditPopComponent, {
      width: '800px',
      data: {
        data: data,
        title: title,
      },
    });

    dialogRef.afterClosed().subscribe((data: PurchasePriceModel) => {
      if (data) {
        data.id = (data.supplier + '__' + data.product)
          .toLowerCase()
          .replace(' ', '_');
        this.databaseService.updateDocument('purchasePrice', { ...data });
      }
    });
  }

  public addPurchase(data?: PurchaseModel) {
    let title = '';
    if (!data) {
      data = new PurchaseModel();
      title = 'Ajouter un achat';
    }
    const dialogRef = this.dialog.open(PurchasePopComponent, {
      width: '800px',
      data: {
        data: data,
        title: title,
      },
    });

    dialogRef.afterClosed().subscribe((data: PurchaseModel) => {
      if (data) {
        const date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
        data.id = (
          date +
          '__' +
          data.product +
          '__' +
          data.supplier +
          '__' +
          data.quantity
        )
          .toLowerCase()
          .replace(' ', '_');
        console.log(data);
        this.databaseService.updateDocument('purchase', { ...data });
      }
    });
  }

  public addLaunderer(data?: LaunderingModel) {
    let title = '';
    if (!data) {
      data = new LaunderingModel();
      title = 'Ajouter un achat';
    }
    const dialogRef = this.dialog.open(LaunderingPopComponent, {
      width: '800px',
      data: {
        data: data,
        title: title,
      },
    });

    dialogRef.afterClosed().subscribe((data: LaunderingModel) => {
      if (data) {
        const date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
        data.id = (date + '__' + data.launderer + '__' + data.totalPrice)
          .toLowerCase()
          .replace(' ', '_');
        this.databaseService.updateDocument('laundering', { ...data });
      }
    });
  }

  public addOrEditBleachingRate(data?: BleachingRateModel) {
    let title = 'Modifier un taux';
    if (!data) {
      data = new BleachingRateModel();
      title = 'Ajouter un taux';
    }
    const dialogRef = this.dialog.open(BleachingRatePopComponent, {
      width: '800px',
      data: {
        data: data,
        title: title,
      },
    });

    dialogRef.afterClosed().subscribe((data: BleachingRateModel) => {
      if (data) {
        data.id = data.launderer.toLowerCase().replace(' ', '_');
        this.databaseService.updateDocument('bleachingRate', { ...data });
      }
    });
  }

  public addSale(data?: SaleModel) {
    let title = '';
    if (!data) {
      data = new SaleModel();
      title = 'Faire une vente';
    }
    const dialogRef = this.dialog.open(SalePopComponent, {
      width: '800px',
      data: {
        data: data,
        title: title,
      },
    });

    dialogRef.afterClosed().subscribe((data: SaleModel) => {
      if (data) {
        const date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
        data.id = (
          date +
          '__' +
          data.name +
          '__' +
          data.product +
          '__' +
          data.quantity +
          '__' +
          data.price
        )
          .toLowerCase()
          .replace(' ', '_');
        this.databaseService.updateDocument('sale', { ...data });
      }
    });
  }
}
