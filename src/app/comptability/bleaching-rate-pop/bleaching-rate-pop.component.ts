import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BleachingRatePopModel } from 'src/app/models/comptability.models';
import { DatabaseService } from 'src/app/services/db.service';

@Component({
  selector: 'app-bleaching-rate-pop',
  templateUrl: './bleaching-rate-pop.component.html',
  styleUrls: ['./bleaching-rate-pop.component.scss']
})
export class BleachingRatePopComponent implements OnInit {
  public launderer: string[] = [];
  public business: string[] = [];
  public title: string = "";

  public profileForm = new FormGroup({
    laundererValidator: new FormControl('', [Validators.required]),
    rateDirtyMoneyValidator: new FormControl('', [Validators.required]),
    bagPriceValidator: new FormControl('', [Validators.required])
  });


  constructor(
    @Inject(MAT_DIALOG_DATA) public inputData: BleachingRatePopModel,
    private databaseService: DatabaseService
  ) { }

  ngOnInit(): void {
    this.profileForm.patchValue(
      {
        laundererValidator: this.inputData.data.launderer,
        rateDirtyMoneyValidator: this.inputData.data.rateDirtyMoney,
        bagPriceValidator: this.inputData.data.bagPrice
      });

    this.title = this.inputData.title
    this.databaseService.getLaunderer().subscribe((output) => {
      this.launderer = output.data;
    });
  }

}
