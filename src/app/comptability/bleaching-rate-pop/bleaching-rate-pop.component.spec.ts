import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BleachingRatePopComponent } from './bleaching-rate-pop.component';

describe('BleachingRatePopComponent', () => {
  let component: BleachingRatePopComponent;
  let fixture: ComponentFixture<BleachingRatePopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BleachingRatePopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BleachingRatePopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
