import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasePopComponent } from './purchase-pop.component';

describe('PurchasePopComponent', () => {
  let component: PurchasePopComponent;
  let fixture: ComponentFixture<PurchasePopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchasePopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasePopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
