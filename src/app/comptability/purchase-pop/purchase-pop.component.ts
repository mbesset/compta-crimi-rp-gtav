import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PurchasePriceModel } from 'src/app/models/comptability.models';
import { DatabaseService } from 'src/app/services/db.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-purchase-pop',
  templateUrl: './purchase-pop.component.html',
  styleUrls: ['./purchase-pop.component.scss'],
})
export class PurchasePopComponent implements OnInit {
  public title: string = '';
  public userMembre: string[] = [];
  public _supplierByBusiness: string[] = [];
  public business: string[] = [];
  public purchasePriceTab: PurchasePriceModel[] = [];

  public profileForm = new FormGroup({
    supplierValidator: new FormControl('', [Validators.required]),
    unitPriceValidator: new FormControl('', [Validators.required]),
    productValidator: new FormControl('', [Validators.required]),
    totalPriceValidator: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    paidByValidator: new FormControl('', [Validators.required]),
    quantityValidator: new FormControl('', [Validators.required]),
  });
  public purchasePriceDataSource: PurchasePriceModel[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public inputData: any,
    private databaseService: DatabaseService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.title = this.inputData.title;

    this.databaseService
      .getDatabase('purchasePrice')
      .subscribe((_purchasePrice: PurchasePriceModel[]) => {
        this.purchasePriceDataSource = _purchasePrice;
      });

    this.databaseService.getBusiness().subscribe((output) => {
      this.business = output.data;
    });

    this.utilsService.getMembre().then((userMembre) => {
      this.userMembre = userMembre;
    });
  }

  /**
   * Recovery of suppliers according to the chosen product
   */
  public getSupplierByBusiness(): void {
    console.log(this.inputData.data.product);
    this.databaseService
      .getCollectionWithFilter(
        'purchasePrice',
        'product',
        '==',
        this.inputData.data.product
      )
      .subscribe((_purchasePrice) => {
        this.purchasePriceTab = _purchasePrice as PurchasePriceModel[];
        this.supplierByBusiness(_purchasePrice as PurchasePriceModel[]);
        this.getPriceByBusinessAndSupplier();
      });
  }

  /**
   * Get supplier
   * @param purchasePrice
   */
  private supplierByBusiness(purchasePrice: PurchasePriceModel[]): void {
    this._supplierByBusiness = purchasePrice.map((_purchasePrice) => {
      return _purchasePrice.supplier;
    });
  }

  /**
   * Price recovery according to supplier and product
   */
  public getPriceByBusinessAndSupplier(): void {
    const purchasePrice: PurchasePriceModel[] = this.purchasePriceTab.filter(
      (obj) => {
        return obj.product === this.inputData.data.product &&
          obj.supplier === this.inputData.data.supplier
          ? true
          : false;
      }
    );
    this.inputData.data.unitPrice = purchasePrice[0]?.unitPrice
      ? purchasePrice[0].unitPrice
      : 0;
  }

  /**
   * Calcul the total price
   */
  public calculTotal(): void {
    this.inputData.data.totalPrice =
      this.inputData.data.unitPrice * this.inputData.data.quantity;
  }
}
