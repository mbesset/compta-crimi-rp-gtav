import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { StorageModelPop } from 'src/app/models/storage.models';

@Component({
  selector: 'app-storage-add-pop',
  templateUrl: './storage-add-pop.component.html',
  styleUrls: ['./storage-add-pop.component.scss']
})
export class StorageAddPopComponent implements OnInit {
  public title: string = '';
  public types: string[] = [];
  public users: string[] = [];
  filteredOptions!: Observable<string[]>;

  public storageForm = new FormGroup({
    nameValidator: new FormControl('', [Validators.required]),
    maxStorageValidator: new FormControl('', [Validators.required])
  });

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public inputData: StorageModelPop,
  ) { }

  ngOnInit(): void {
    this.title = this.inputData.title;
  }
}
