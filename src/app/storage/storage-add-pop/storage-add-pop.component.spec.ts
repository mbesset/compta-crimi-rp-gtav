import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageAddPopComponent } from './storage-add-pop.component';

describe('StorageAddPopComponent', () => {
  let component: StorageAddPopComponent;
  let fixture: ComponentFixture<StorageAddPopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorageAddPopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageAddPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
