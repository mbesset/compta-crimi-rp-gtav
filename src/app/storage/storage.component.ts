import { Component, OnInit } from '@angular/core';
import { StockModel, StorageModel, StorageModelPop, TenantModel } from '../models/storage.models';
import { DatabaseService } from '../services/db.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { StorageAddPopComponent } from './storage-add-pop/storage-add-pop.component';
import { MatDialog } from '@angular/material/dialog';
import { DeletePopupComponent } from '../utils/delete-popup/delete-popup.component';
import { UtilsService } from '../services/utils.service';


@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']

})
export class StorageComponent implements OnInit {
  public storages: StorageModel[] = [];
  public newStock: string[] = [];
  public users: string[] = [];
  public userMembre: string[] = [];

  constructor(
    private databaseService: DatabaseService,
    private matSnackBar: MatSnackBar,
    public dialog: MatDialog,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.databaseService.getDatabase("storage").subscribe((_storage: StorageModel[]) => {
      this.storages = _storage;
      // Init array is undefined 
      this.storages.map((e) => {
        e.stock = e.stock ? e.stock : [];
        e.tenant = e.tenant ? e.tenant : [];
      })
    });

    this.utilsService.getMembre().then((userMembre: any) => {
      this.userMembre = userMembre;
    });
  }

  /**
   * Add item on stock
   * @param storage 
   */
  public addItem(storage: StorageModel) {
    const emptyArray = storage.stock.filter((e) => {
      return e.name === ""
    });
    if (emptyArray.length === 0) {
      storage.stock.push({ name: "", quantity: 0, edit: true });
    }
  }

  /**
   * Save Data
   * @param storage 
   * @param stock 
   */
  public save(storage: StorageModel, stock: StockModel) {
    if (stock.name.length >= 3) {
      const exist = storage.stock.filter((e) => {
        return e.name === stock.name
      });
      if (exist.length <= 1) {
        stock.edit = false;
        delete stock?.edit;
        this.databaseService.updateDocument('storage', storage, storage.id);
      } else {
        this.matSnackBar.open("Item déja présent dans le stock");
      }
    } else {
      this.matSnackBar.open("Nom de l'item trop court (3 caracts min)");
    }
  }

  /**
   * Delete element on array
   * @param storage 
   * @param stock 
   */
  public delete(storage: StorageModel, stock: StockModel) {
    storage.stock = storage.stock.filter((ele: StockModel) => {
      return ele.name != stock.name;
    });
    this.databaseService.updateDocument('storage', storage, storage.id);
  }

  /**
   * 
   */
  public addStorage() {
    const dialogRef = this.dialog.open(StorageAddPopComponent, {
      width: '800px',
      data: {
        data: new StorageModel(),
        title: "Ajouter un appartement",
      },
    });

    dialogRef.afterClosed().subscribe((data: StorageModel) => {
      data.id = (data.storageName).toLowerCase().trim().replace(' ', '_');
      this.databaseService.updateDocument('storage', { ...data });
    });
  }

  /**
   * 
   * @param storage 
   */
  public deleteStorage(storage: StorageModel) {
    const dialogRef = this.dialog.open(DeletePopupComponent, {
      width: '800px',
      data: {
        title: "Supprimer un appartement",
        message: "Etes vous sur de vouloir supprimer l'appartement ?",
        data: storage
      },
    });

    dialogRef.afterClosed().subscribe((data: StorageModel) => {
      this.databaseService.deleteDocument("storage", storage);
    });
  }


  /**
   * 
   * @param storage 
   * @param tenant 
   */
  deleteTenant(storage: StorageModel, tenant: TenantModel) {
    storage.tenant = storage.tenant.filter((ele: TenantModel) => {
      return ele.name != tenant.name;
    });
    this.databaseService.updateDocument('storage', storage, storage.id);
  }


  /**
   * 
   * @param storage 
   * @param tenant 
   */
  saveTenant(storage: StorageModel, tenant: TenantModel) {
    const exist = storage.tenant.filter((e) => {
      return e.name === tenant.name
    });
    if (exist.length <= 1) {
      tenant.edit = false;
      delete tenant?.edit;
      this.databaseService.updateDocument('storage', storage, storage.id);
    } else {
      this.matSnackBar.open("Déja collocataire");
    }

  }

  /**
   * 
   * @param storage 
   */
  public addTenant(storage: StorageModel) {
    if (storage.tenant.length < 4) {
      const emptyArray = storage.tenant.filter((e) => {
        return e.name === ""
      });
      if (emptyArray.length === 0) {
        storage.tenant.push({ name: "", edit: true });
      }
    } else {
      this.matSnackBar.open("4 locataires max par habitation");
    }

  }
}
