import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatabaseService } from '../../services/db.service';
import { Observable } from 'rxjs';
import { AnnuaireModel } from '../../models/annuaire.model';
import { UtilsService } from '../../services/utils.service';

export interface DialogData {
  data: any;
  title: string;
  message: string;
}

@Component({
  selector: 'app-payment-add-pop',
  templateUrl: './payment-add-pop.component.html',
  styleUrls: ['./payment-add-pop.component.scss'],
})
export class PaymentAddPopComponent implements OnInit {
  public title: string = '';
  public types: string[] = [];
  public userInfo: AnnuaireModel = new AnnuaireModel();
  myControl = new FormControl();
  public users: string[] = [];
  filteredOptions!: Observable<string[]>;
  public calculData = {
    drugPaid: 0,
    sacPaid: 0,
  };
  public userMembre: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public inputData: DialogData,
    private databaseService: DatabaseService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.title = this.inputData.title;
    const id: string = this.inputData.data.name.replace(' ', '_').toLowerCase();
    this.databaseService.getDocument('annuaire', id).subscribe((_userInfo) => {
      this.userInfo = _userInfo;
      this.drugPaidCalcul();
      this.sacPaidCalcul();
    });
    this.utilsService.getMembre().then((userMembre) => {
      this.userMembre = userMembre;
    });
  }

  public drugPaidCalcul(): void {
    this.calculData.drugPaid =
      this.inputData.data.dirtyMoney *
      ((this.userInfo.percentDeal as number) / 100);
    this.updateTotal();
  }

  public sacPaidCalcul(): void {
    this.calculData.sacPaid =
      this.inputData.data.superetteBag * (this.userInfo.other as number);
    this.updateTotal();
  }

  public updateTotal(): number {
    return (this.inputData.data.paid =
      this.calculData.sacPaid + this.calculData.drugPaid);
  }
}
