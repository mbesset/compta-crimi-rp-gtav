import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAddPopComponent } from './payment-add-pop.component';

describe('PaymentAddPopComponent', () => {
  let component: PaymentAddPopComponent;
  let fixture: ComponentFixture<PaymentAddPopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentAddPopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAddPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
