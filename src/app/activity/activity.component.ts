import { Component, OnInit, ViewChild } from '@angular/core';
import { AnnuaireModel } from '../models/annuaire.model';
import { ActivityModel } from "../models/activity.models"
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { DatabaseService } from '../services/db.service';
import { ActivityAddPopComponent } from "./activity-add-pop/activity-add-pop.component"
import { formatDate } from '@angular/common';
import { PaymentAddPopComponent } from "./payment-add-pop/payment-add-pop.component"

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns: string[] = [
    'name',
    'quantity',
    'product',
    'actions',
  ];
  dataSource = new MatTableDataSource<AnnuaireModel>([]);

  constructor(
    public dialog: MatDialog,
    private databaseService: DatabaseService,
  ) { }

  ngOnInit(): void {
    this.updateTable();
  }

  /**
   * Open Dialog for add contact
   */
  public add() {
    const data: ActivityModel = new ActivityModel();
    const dialogRef = this.dialog.open(ActivityAddPopComponent, {
      width: '800px',
      data: {
        data: data,
        title: "Ajouter un travailleur",
        message: '',
      },
    });

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        data.createAt = formatDate(new Date(), 'yyyy/MM/dd', 'en')
        this.databaseService.addDocument('activity', data);
      }
    });
  }

  /**
   * Open Dialog for edit contact
   * @param element 
   */
  public paid(element: AnnuaireModel) {
    const dialogRef = this.dialog.open(PaymentAddPopComponent, {
      width: '800px',
      data: {
        data: element,
        title: "Payer le contact",
        message: '',
      },
    });

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        data.actif = false;
        this.databaseService.updateDocument('activity', data);
      }
    });
  }

  /**
   * Update Table DATA
   */
  private updateTable() {
    this.databaseService.getCollectionWithFilter('activity', "actif", "==", true).subscribe((result: any) => {
      this.dataSource = new MatTableDataSource<AnnuaireModel>(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  /**
   * Apply Filter on table
   * @param event 
   */
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
