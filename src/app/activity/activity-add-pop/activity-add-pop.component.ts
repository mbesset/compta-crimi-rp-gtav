import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatabaseService } from '../../services/db.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AnnuaireModel } from '../../models/annuaire.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

export interface DialogData {
  data: any;
  title: string;
  message: string;
}

@Component({
  selector: 'app-activity-add-pop',
  templateUrl: './activity-add-pop.component.html',
  styleUrls: ['./activity-add-pop.component.scss'],
})
export class ActivityAddPopComponent implements OnInit {
  public title: string = '';
  public types: string[] = [];
  public users: string[] = [];
  filteredOptions: Observable<string[]> | undefined;

  public activityFormGroup = new FormGroup({
    nameValidator: new FormControl('', [Validators.required]),
    typeValidator: new FormControl('', [Validators.required]),
    quantityValidator: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public inputData: DialogData,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.title = this.inputData.title;

    this.databaseService.getDatabase('annuaire').subscribe((result) => {
      this.users = result.map((e: AnnuaireModel) => {
        return e.name + ' ' + e.surName;
      });

      this.filteredOptions = this.activityFormGroup
        .get('nameValidator')
        ?.valueChanges.pipe(
          startWith(''),
          map((value) => this._filter(value))
        ) as Observable<string[]>;
    });

    this.databaseService.getBusiness().subscribe((result: any) => {
      this.types = result.data;
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.users.filter((users) =>
      users.toLowerCase().includes(filterValue)
    );
  }
}
