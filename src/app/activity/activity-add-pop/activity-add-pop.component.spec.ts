import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityAddPopComponent } from './activity-add-pop.component';

describe('ActivityAddPopComponent', () => {
  let component: ActivityAddPopComponent;
  let fixture: ComponentFixture<ActivityAddPopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivityAddPopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityAddPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
