import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsModel } from 'src/app/models/settings.models';
import { DatabaseService } from 'src/app/services/db.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  public settings: SettingsModel = new SettingsModel();
  constructor(
    private router: Router,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.databaseService.getSettings().subscribe((settings: any) => {
      this.settings = {
        ...this.settings,
        ...settings,
      };
    });
  }

  public navigate(target: string) {
    this.router.navigate([target]);
  }
}
