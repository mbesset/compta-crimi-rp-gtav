import { Component, OnInit } from '@angular/core';
import { SettingsModel } from 'src/app/models/settings.models';
import { DatabaseService } from 'src/app/services/db.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public settings: SettingsModel = new SettingsModel();

  constructor(
    private databaseService: DatabaseService,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.titleService.setTitle(this.settings.webSiteTitle);
    this.databaseService.getSettings().subscribe((settings: any) => {
      this.settings = {
        ...this.settings,
        ...settings,
      };
      this.titleService.setTitle(this.settings.webSiteTitle);
    });
  }
}
