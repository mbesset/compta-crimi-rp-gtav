import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayHistoricComponent } from './day-historic.component';

describe('DayHistoricComponent', () => {
  let component: DayHistoricComponent;
  let fixture: ComponentFixture<DayHistoricComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayHistoricComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
