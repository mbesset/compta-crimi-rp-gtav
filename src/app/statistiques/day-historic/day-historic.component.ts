import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DatabaseService } from '../../services/db.service';

@Component({
  selector: 'app-day-historic',
  templateUrl: './day-historic.component.html',
  styleUrls: ['./day-historic.component.scss']
})
export class DayHistoricComponent implements OnInit {
  myMap = new Map();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns: string[] = [
    'name',
    'totalPaid',
    'totalSuperetteBag',
    'totalDirtyMoney'

  ];
  dataSource = new MatTableDataSource<any>([]);

  constructor(
    private databaseService: DatabaseService
  ) { }

  ngOnInit(): void {
    const currentDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');
    this.databaseService.getCollectionWithFilter("activity", "createAt", "==", currentDate).subscribe((e) => {
      e.forEach((a: any) => {
        if (this.myMap.get(a.name)) {
          const currentValue = this.myMap.get(a.name);
          this.myMap.set(a.name, {
            totalPaid: a.paid + currentValue.totalPaid,
            totalSuperetteBag: a.superetteBag + currentValue.totalSuperetteBag,
            totalDirtyMoney: a.dirtyMoney + currentValue.totalDirtyMoney
          })
        } else {
          this.myMap.set(a.name,
            {
              totalPaid: a.paid || 0,
              totalSuperetteBag: a.superetteBag || 0,
              totalDirtyMoney: a.dirtyMoney || 0
            })
        }
      });
      const array = Array.from(this.myMap, ([name, value]) => (
        {
          name: name,
          totalPaid: value.totalPaid,
          totalSuperetteBag: value.totalSuperetteBag,
          totalDirtyMoney: value.totalDirtyMoney
        }));
      this.dataSource = new MatTableDataSource<any>(array);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  /**
   * Apply Filter on table
   * @param event 
   */
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
